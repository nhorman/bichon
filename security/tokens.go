// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019-2020 Red Hat, Inc.

package security

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"os"
	"runtime"

	"github.com/godbus/dbus"
	log "github.com/sirupsen/logrus"
	"github.com/zalando/go-keyring"
	"golang.org/x/crypto/nacl/secretbox"
)

var serviceName = "bichon"
var tokenEncryptionKey = "token-encryption-key"

func GenerateMasterKey() ([32]byte, error) {
	var key [32]byte
	_, err := io.ReadFull(rand.Reader, key[:])
	return key, err
}

func StoreMasterKey(key [32]byte) error {
	data64 := base64.StdEncoding.EncodeToString(key[:])

	return keyring.Set(serviceName, tokenEncryptionKey, data64)
}

func EnsureMasterKey() ([32]byte, bool, error) {
	key, err := FetchMasterKey()
	newkey := false
	if err == keyring.ErrNotFound {
		key, err = GenerateMasterKey()
		if err != nil {
			return [32]byte{}, false, err
		}

		err = StoreMasterKey(key)
		if err != nil {
			return [32]byte{}, false, err
		}

		newkey = true
	}

	return key, newkey, nil
}

func FetchMasterKey() ([32]byte, error) {
	data64, err := keyring.Get(serviceName, tokenEncryptionKey)

	if err != nil {
		return [32]byte{}, err
	}

	var key [32]byte

	data, err := base64.StdEncoding.DecodeString(data64)
	if err != nil {
		return [32]byte{}, err
	}

	copy(key[:], data[:32])

	return key, nil
}

func EncryptToken(token string, masterkey [32]byte) (string, error) {
	var nonce [24]byte
	_, err := io.ReadFull(rand.Reader, nonce[:])
	if err != nil {
		return "", err
	}

	ciphertext := secretbox.Seal(nonce[:], []byte(token), &nonce, &masterkey)

	ciphertext64 := base64.StdEncoding.EncodeToString(ciphertext)

	return ciphertext64, nil
}

func DecryptToken(ciphertext64 string, masterkey [32]byte) (string, error) {
	ciphertext, err := base64.StdEncoding.DecodeString(ciphertext64)
	if err != nil {
		return "", err
	}

	var nonce [24]byte
	copy(nonce[:], ciphertext[:24])
	token, ok := secretbox.Open(nil, ciphertext[24:], &nonce, &masterkey)
	if !ok {
		return "", fmt.Errorf("Unable to decrypt token %s", ciphertext64)
	}

	return string(token), nil
}

func FetchToken(server, project string) (string, bool, error) {
	keyname := fmt.Sprintf("%s/%s", server, project)

	log.Infof("Getting token '%s'", keyname)
	data, err := keyring.Get(serviceName, keyname)
	globalToken := false
	if err != nil {
		log.Infof("Getting token '%s'", server)
		data, err = keyring.Get(serviceName, server)
		if err == nil {
			globalToken = true
		}
	}

	log.Infof("Ok '%s' global %t err %s", data, globalToken, err)
	return data, globalToken, err
}

func StoreToken(server, project, data string) error {
	var keyname string
	if project == "" {
		keyname = server
	} else {
		keyname = fmt.Sprintf("%s/%s", server, project)
	}

	log.Infof("Setting token '%s'", keyname)
	return keyring.Set(serviceName, keyname, data)
}

func KeyringIsAvailable() bool {
	log.Infof("Checking if keyring is available on %s", runtime.GOOS)
	// go-keyring has support for these platforms
	if runtime.GOOS == "windows" || runtime.GOOS == "darwin" {
		return true
	}

	// go-keyring has no support for these platforms
	if runtime.GOOS != "linux" {
		return false
	}

	// If DBUS_SESSION_BUS_ADDRESS isn't set, then in theory
	// DBus session bus will be auto-spawned, however, this
	// doesn't work reliably. gnome-keyring will be attached
	// to the first auto-spawned session bus. If you login a
	// a second time a new session bus is spawned and will not
	// be able to use gnome-keyring. Even if gnome-keyring
	// is auto-spawned, it is likely nothing will be providing
	// a password prompt for unlocking the keyring
	if os.Getenv("DBUS_SESSION_BUS_ADDRESS") == "" {
		log.Infof("No DBus session bus address found")
		return false
	}

	conn, err := dbus.SessionBus()
	if err != nil {
		log.Infof("Cannot connect to session bus: %s", err)
		return false
	}

	var names []string

	err = conn.BusObject().Call("org.freedesktop.DBus.ListActivatableNames", 0).Store(&names)
	if err != nil {
		log.Infof("Cannot list names on session bus: %s", err)
		return false
	}

	hasKeyring := false
	for _, name := range names {
		if name == "org.gnome.keyring" {
			log.Infof("Found activatable keyring")
			hasKeyring = true
		}
		// Don't check for Prompter because if it isn't
		// running we cannot assume activating it will
		// work without a full desktop env being present
	}

	err = conn.BusObject().Call("org.freedesktop.DBus.ListNames", 0).Store(&names)
	if err != nil {
		log.Infof("Cannot list names on session bus: %s", err)
		return false
	}
	hasPrompter := false
	for _, name := range names {
		if name == "org.gnome.keyring" {
			log.Infof("Found running keyring")
			hasKeyring = true
		} else if name == "org.gnome.keyring.SystemPrompter" {
			log.Infof("Found running keyring prompter")
			hasPrompter = true
		}
	}

	return hasPrompter && hasKeyring
}
