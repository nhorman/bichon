// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package cache

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
	"time"

	"gitlab.com/bichon-project/bichon/model"
)

func exists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

func testMergeReq(repo *model.Repo, id uint) model.MergeReq {
	v1 := model.Series{
		Index:   34643,
		Version: 1,
		Patches: []model.Commit{
			model.Commit{
				Hash:  "b7e0c7e4ca23c40c13fc7dee3aed4f8a33af48b6",
				Title: "Do something",
				Author: model.User{
					Name:  "Fred",
					Email: "fred@example.org",
				},
				CreatedAt: time.Date(2019, 4, 14, 8, 15, 2, 0, time.UTC),
				Committer: model.User{
					Name:  "Emily",
					Email: "emily@example.org",
				},
				UpdatedAt: time.Date(2019, 4, 16, 07, 45, 21, 0, time.UTC),
				Message:   "A detailed message",
			},
		},
	}
	v2 := model.Series{
		Index:   3532,
		Version: 2,
		Patches: []model.Commit{
			model.Commit{
				Hash:  "b7e0c7e4ca23c40c13fc7dee3aed4f8a33af48b6",
				Title: "Do something",
				Author: model.User{
					Name:  "Fred",
					Email: "fred@example.org",
				},
				CreatedAt: time.Date(2019, 4, 14, 8, 15, 2, 0, time.UTC),
				Committer: model.User{
					Name:  "Emily",
					Email: "emily@example.org",
				},
				UpdatedAt: time.Date(2019, 4, 16, 7, 45, 21, 0, time.UTC),
				Message:   "A detailed message",
			},
			model.Commit{
				Hash:  "5dfeb6cc6c3034b7c56eacacec1f50ee9b8edf1f",
				Title: "Do something",
				Author: model.User{
					Name:  "Fred",
					Email: "fred@example.org",
				},
				CreatedAt: time.Date(2019, 4, 17, 8, 16, 24, 0, time.UTC),
				Committer: model.User{
					Name:  "Emily",
					Email: "emily@example.org",
				},
				UpdatedAt: time.Date(2019, 4, 17, 8, 45, 25, 0, time.UTC),
				Message:   "A detailed message",
			},
		},
	}

	return model.MergeReq{
		Repo:      *repo,
		ID:        id,
		Title:     "Implement some stuff",
		CreatedAt: time.Date(2019, 4, 24, 15, 24, 23, 0, time.UTC),
		UpdatedAt: time.Date(2019, 4, 28, 12, 54, 17, 0, time.UTC),
		Submitter: model.Account{
			UserName: "fred",
			RealName: "Fred",
		},
		Description: "Do some stuff",
		Versions: []model.Series{
			v1, v2,
		},
	}
}

func TestFileCacheAddRemoveRepo(t *testing.T) {
	root, err := ioutil.TempDir("", "bichon-filecache-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(root)

	cache := NewFileCache(root)

	repo := &model.Repo{
		Directory: "/does-not-exist",
		Remote:    "origin",
		Server:    "gitlab.com",
		Project:   "bichon/bichon-test",
	}

	fileCache, ok := cache.(*fileCache)
	if !ok {
		t.Error("Expected a file cache instance")
	}

	expectedDir := fileCache.repoDir(repo)
	defer os.Remove(expectedDir)

	err = cache.AddRepo(repo)
	if err != nil {
		t.Fatal(err)
	}
	if !exists(expectedDir) {
		t.Errorf("Expected to see dir %s", expectedDir)
	}

	/* Double create is not an error */
	err = cache.AddRepo(repo)
	if err != nil {
		t.Fatal(err)
	}
	if !exists(expectedDir) {
		t.Errorf("Expected to see dir %s", expectedDir)
	}

	cache.RemoveRepo(repo)
	if exists(expectedDir) {
		t.Errorf("Did not expect to see dir %s", expectedDir)
	}

	/* Double delete is not an error */
	cache.RemoveRepo(repo)
	if exists(expectedDir) {
		t.Errorf("Did not expect to see dir %s", expectedDir)
	}
}

func TestFileCacheMergeRequests(t *testing.T) {
	root, err := ioutil.TempDir("", "bichon-filecache-test")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(root)

	cache := NewFileCache(root)

	repo := &model.Repo{
		Directory: "/does-not-exist",
		Remote:    "origin",
		Server:    "gitlab.com",
		Project:   "bichon/bichon-test",
	}

	fileCache, ok := cache.(*fileCache)
	if !ok {
		t.Error("Expected a file cache instance")
	}

	err = cache.AddRepo(repo)
	if err != nil {
		t.Fatal(err)
	}

	ids, err := cache.ListMergeRequests(repo)
	if err != nil {
		t.Fatal(err)
	}
	if len(ids) != 0 {
		t.Errorf("Got %d IDs but expected 0", len(ids))
	}

	expectedDir := fileCache.repoDir(repo)

	notjson := filepath.Join(expectedDir, "34.xml")
	err = ioutil.WriteFile(notjson, []byte("<demo/>"), 0600)
	if err != nil {
		t.Fatal(err)
	}

	notnum := filepath.Join(expectedDir, "hello.json")
	err = ioutil.WriteFile(notnum, []byte("{}"), 0600)
	if err != nil {
		t.Fatal(err)
	}

	ids, err = cache.ListMergeRequests(repo)
	if err != nil {
		t.Fatal(err)
	}
	if len(ids) != 0 {
		t.Errorf("Got %d IDs but expected 0", len(ids))
	}

	mreq23 := testMergeReq(repo, 23)
	mreq67 := testMergeReq(repo, 67)

	err = cache.SaveMergeRequest(&mreq23)
	if err != nil {
		t.Fatal(err)
	}
	mreq23file := filepath.Join(expectedDir, "23.json")
	if !exists(mreq23file) {
		t.Errorf("File %s does not exist", mreq23file)
	}

	err = cache.SaveMergeRequest(&mreq67)
	if err != nil {
		t.Fatal(err)
	}
	mreq67file := filepath.Join(expectedDir, "67.json")
	if !exists(mreq23file) {
		t.Errorf("File %s does not exist", mreq67file)
	}

	ids, err = cache.ListMergeRequests(repo)
	if err != nil {
		t.Fatal(err)
	}
	if len(ids) != 2 {
		t.Errorf("Got %d IDs but expected 2", len(ids))
	}

	if ids[0] != 23 {
		t.Errorf("Got ID %d but expected 23", ids[0])
	}
	if ids[1] != 67 {
		t.Errorf("Got ID %d but expected 67", ids[1])
	}

	mreq67new, err := cache.LoadMergeRequest(repo, 67)
	if err != nil {
		t.Fatal(err)
	}

	j1, err := mreq67.ToJSON()
	if err != nil {
		t.Fatal(err)
	}

	j2, err := mreq67new.ToJSON()
	if err != nil {
		t.Fatal(err)
	}

	if string(j1) != string(j2) {
		t.Errorf("Expected %s but got %s", j1, j2)
	}

	_, err = cache.LoadMergeRequest(repo, 123)
	if err == nil {
		t.Errorf("Unexpectedly loaded merge request 123")
	}

	err = cache.RemoveRepo(repo)
	if err != nil {
		t.Fatal(err)
	}

	_, err = cache.LoadMergeRequest(repo, 67)
	if err == nil {
		t.Errorf("Unexpectedly loaded merge request 67")
	}

}
