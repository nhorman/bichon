// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"strconv"
	"time"
	"unicode"

	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/model"
)

type FilterFormListener interface {
	FilterFormConfirm(model.MergeReqFilter)
	FilterFormCancel()
}

type FilterForm struct {
	tview.Primitive

	Form     *tview.Form
	Listener FilterFormListener
	Projects *tview.DropDown

	StateOpen   bool
	StateClosed bool
	StateLocked bool
	StateMerged bool

	StatusNew     bool
	StatusUpdated bool
	StatusOld     bool
	StatusRead    bool

	Project         string
	AgeUnit         time.Duration
	AgeVal          time.Duration
	AuthorRegex     string
	AuthorFixedCase bool
	Label           string
}

func (form *FilterForm) addCheckbox(label, text string, val bool, changed func(checked bool)) {
	cbox := NewCheckbox(text).
		SetLabel(label).
		SetChecked(val).
		SetChangedFunc(changed)
	form.Form.AddFormItem(cbox)
}

func (form *FilterForm) cancelFunc() {
	form.Form.SetFocus(0)
	form.Listener.FilterFormCancel()
}

func NewFilterForm(listener FilterFormListener) *FilterForm {

	form := &FilterForm{
		Form:     tview.NewForm(),
		Listener: listener,
		Projects: tview.NewDropDown(),

		StateOpen: true,
	}

	form.Primitive = Modal(form.Form, 50, 33)

	form.Form.SetCancelFunc(form.cancelFunc)

	form.Form.SetBorder(true).
		SetTitle("Merge request filtering")

	form.Form.AddFormItem(form.Projects)
	form.Projects.SetLabel("Project")
	form.refreshMain([]model.Repo{})

	form.addCheckbox("State", "Opened", true, func(val bool) {
		form.StateOpen = val
	})
	form.addCheckbox("", "Closed", false, func(val bool) {
		form.StateClosed = val
	})
	form.addCheckbox("", "Merged", false, func(val bool) {
		form.StateMerged = val
	})
	form.addCheckbox("", "Locked", false, func(val bool) {
		form.StateLocked = val
	})

	form.addCheckbox("Status", "New", false, func(val bool) {
		form.StatusNew = val
	})
	form.addCheckbox("", "Updated", false, func(val bool) {
		form.StatusUpdated = val
	})
	form.addCheckbox("", "Old", false, func(val bool) {
		form.StatusOld = val
	})
	form.addCheckbox("", "Read", false, func(val bool) {
		form.StatusRead = val
	})

	form.Form.AddInputField("Age", "", 10,
		func(text string, char rune) bool {
			return unicode.IsDigit(char)
		},
		func(text string) {
			val, _ := strconv.Atoi(text)
			form.AgeVal = time.Duration(val)
		})
	form.Form.AddDropDown("", []string{
		"unlimited",
		"mins",
		"hours",
		"days",
		"weeks",
		"months",
		"years",
	}, 0, func(option string, idx int) {
		units := []time.Duration{
			0,
			time.Minute,
			time.Hour,
			time.Hour * 24,
			time.Hour * 24 * 7,
			time.Hour * 24 * 31,
			time.Hour * 24 * 365,
		}
		form.AgeUnit = units[idx]
	})

	form.Form.AddInputField("Author regex", "", 20, nil,
		func(text string) {
			form.AuthorRegex = text
		})
	form.addCheckbox("", "Case sensitive", false, func(val bool) {
		form.AuthorFixedCase = val
	})
	form.Form.AddInputField("Label", "", 20, nil,
		func(text string) {
			form.Label = text
		})
	form.Form.AddButton("Apply", func() {
		form.Form.SetFocus(0)
		filter := form.GetMergeReqFilter()
		form.Listener.FilterFormConfirm(filter)
	})
	form.Form.AddButton("Cancel", form.cancelFunc)

	return form
}

func (form *FilterForm) Refresh(app *tview.Application, repos []model.Repo) {
	go app.QueueUpdateDraw(func() {
		form.refreshMain(repos)
	})
}

func (form *FilterForm) refreshMain(repos []model.Repo) {
	projects := []string{"-any-"}
	for _, repo := range repos {
		projects = append(projects, repo.Project)
	}
	form.Projects.SetOptions(projects,
		func(text string, idx int) {
			if idx == 0 {
				form.Project = ""
			} else {
				form.Project = text
			}
		})
	form.Projects.SetCurrentOption(0)
}

func (form *FilterForm) GetMergeReqFilter() model.MergeReqFilter {
	and := func(orig, new model.MergeReqFilter) model.MergeReqFilter {
		if orig != nil {
			return model.MergeReqFilterBoth(orig, new)
		} else {
			return new
		}
	}
	or := func(orig, new model.MergeReqFilter) model.MergeReqFilter {
		if orig != nil {
			return model.MergeReqFilterEither(orig, new)
		} else {
			return new
		}
	}

	var filter model.MergeReqFilter

	if form.Project != "" {
		filter = and(filter,
			model.MergeReqFilterProject(form.Project))
	}

	var stateFilter model.MergeReqFilter
	if form.StateOpen {
		stateFilter = or(stateFilter,
			model.MergeReqFilterState(model.STATE_OPENED))
	}
	if form.StateClosed {
		stateFilter = or(stateFilter,
			model.MergeReqFilterState(model.STATE_CLOSED))
	}
	if form.StateMerged {
		stateFilter = or(stateFilter,
			model.MergeReqFilterState(model.STATE_MERGED))
	}
	if form.StateLocked {
		stateFilter = or(stateFilter,
			model.MergeReqFilterState(model.STATE_LOCKED))
	}

	if stateFilter != nil {
		filter = and(filter, stateFilter)
	}

	var statusFilter model.MergeReqFilter
	if form.StatusNew {
		statusFilter = or(statusFilter,
			model.MergeReqFilterMetadataStatus(model.STATUS_NEW))
	}
	if form.StatusUpdated {
		statusFilter = or(statusFilter,
			model.MergeReqFilterMetadataStatus(model.STATUS_UPDATED))
	}
	if form.StatusOld {
		statusFilter = or(statusFilter,
			model.MergeReqFilterMetadataStatus(model.STATUS_OLD))
	}
	if form.StatusRead {
		statusFilter = or(statusFilter,
			model.MergeReqFilterMetadataStatus(model.STATUS_READ))
	}

	if statusFilter != nil {
		filter = and(filter, statusFilter)
	}

	if form.AgeUnit != 0 && form.AgeVal != 0 {
		age := form.AgeUnit * form.AgeVal
		filter = and(filter,
			model.MergeReqFilterAge(age))
	}

	if form.AuthorRegex != "" {
		re := ".*" + form.AuthorRegex + ".*"
		if !form.AuthorFixedCase {
			re = "(?i)" + re
		}
		filter = and(filter,
			model.MergeReqFilterSubmitterRealName(re))
	}

	if form.Label != "" {
		filter = and(filter, model.MergeReqFilterLabels(form.Label))
	}

	return filter
}
