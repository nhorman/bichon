// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"crypto/tls"
	"fmt"
	"strings"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	log "github.com/sirupsen/logrus"

	"gitlab.com/bichon-project/bichon/config"
	"gitlab.com/bichon-project/bichon/controller"
	"gitlab.com/bichon-project/bichon/model"
	"gitlab.com/bichon-project/bichon/security"
	"gitlab.com/bichon-project/bichon/view/distractions/pacman"
	"gitlab.com/bichon-project/bichon/view/distractions/snake"
)

type Display struct {
	*tview.Application

	TokenKey [32]byte
	Config   *config.AppConfig

	Engine    controller.Engine
	MergeReqs model.MergeReqList
	Repos     []model.Repo

	Layout       *tview.Flex
	KeyShortcuts *tview.TextView
	StatusBar    *tview.TextView
	Messages     *MessageBar
	Pages        *tview.Pages
	Index        *IndexPage
	Detail       *DetailPage
	Projects     *ProjectsPage
	AddProject   *AddProjectPage
	ThisPage     Page

	Snake  *snake.Game
	Pacman *pacman.Game

	FormActive         bool
	ChoosePasswordForm *ChoosePasswordForm
	PasswordForm       *PasswordForm
	SortForm           *SortForm
	FilterForm         *FilterForm
}

func NewDisplay(tlscfg *tls.Config) (*Display, error) {
	err := LoadStyleConfig()
	if err != nil {
		return nil, err
	}

	tview.TabSize = 8
	tview.Styles.PrimitiveBackgroundColor = GetStyleColor(ELEMENT_PRIMITIVE_FILL)
	tview.Styles.ContrastBackgroundColor = GetStyleColor(ELEMENT_CONTRAST_FILL)
	tview.Styles.MoreContrastBackgroundColor = GetStyleColor(ELEMENT_MORE_CONTRAST_FILL)
	tview.Styles.PrimaryTextColor = GetStyleColor(ELEMENT_PRIMARY_TEXT)
	tview.Styles.SecondaryTextColor = GetStyleColor(ELEMENT_SECONDARY_TEXT)
	tview.Styles.TertiaryTextColor = GetStyleColor(ELEMENT_TERTIARY_TEXT)
	tview.Styles.InverseTextColor = GetStyleColor(ELEMENT_INVERSE_TEXT)
	tview.Styles.ContrastSecondaryTextColor = GetStyleColor(ELEMENT_CONTRAST_SECONDARY_TEXT)
	tview.Styles.BorderColor = GetStyleColor(ELEMENT_BORDER)
	tview.Styles.TitleColor = GetStyleColor(ELEMENT_TITLE)
	tview.Styles.GraphicsColor = GetStyleColor(ELEMENT_GRAPHICS)

	cfg, err := config.LoadAppConfig()
	if err != nil {
		return nil, err
	}

	msgs := NewMessageBar()
	display := &Display{
		Application:  tview.NewApplication(),
		Config:       cfg,
		Layout:       tview.NewFlex(),
		KeyShortcuts: tview.NewTextView().SetDynamicColors(true),
		StatusBar:    tview.NewTextView().SetDynamicColors(true),
		Messages:     msgs,
		Pages:        tview.NewPages(),
	}

	display.Engine = controller.NewEngine(display, tlscfg)

	display.Index = NewIndexPage(display.Application, display)
	display.Detail = NewDetailPage(display.Application, display)
	display.Projects = NewProjectsPage(display.Application, display)
	display.AddProject = NewAddProjectPage(display.Application, display)
	display.ChoosePasswordForm = NewChoosePasswordForm(display)
	display.PasswordForm = NewPasswordForm(display)
	display.SortForm = NewSortForm(display, SORT_FORM_ORDER_AGE, false)
	display.FilterForm = NewFilterForm(display)
	display.Snake = snake.NewGame(display.Application, display)
	display.Pacman = pacman.NewGame(display.Application, display)

	display.SetRoot(display.Layout, true)

	display.Layout.SetDirection(tview.FlexRow).
		AddItem(display.KeyShortcuts, 1, 1, false).
		AddItem(display.Pages, 0, 1, false).
		AddItem(display.StatusBar, 1, 1, false).
		AddItem(display.Messages.Text, 1, 1, false)

	display.Pages.AddPage(display.Index.GetName(), display.Index, true, true)
	display.Pages.AddPage(display.Detail.GetName(), display.Detail, true, true)
	display.Pages.AddPage(display.Projects.GetName(), display.Projects, true, true)
	display.Pages.AddPage(display.AddProject.GetName(), display.AddProject, true, true)
	display.Pages.AddPage("new-password-form", display.ChoosePasswordForm, true, false)
	display.Pages.AddPage("password-form", display.PasswordForm, true, false)
	display.Pages.AddPage("sort-form", display.SortForm, true, false)
	display.Pages.AddPage("filter-form", display.FilterForm, true, false)
	display.Pages.AddPage("snake", display.Snake, true, true)
	display.Pages.AddPage("pacman", display.Pacman, true, true)

	display.MergeReqs.Filter = display.FilterForm.GetMergeReqFilter()
	display.MergeReqs.Sorter = display.SortForm.GetMergeReqSorter()

	display.StatusBar.SetText(fmt.Sprintf("[%s:%s]--- Bichon",
		GetStyleColorName(ELEMENT_STATUS_TEXT),
		GetStyleColorName(ELEMENT_STATUS_FILL)) +
		strings.Repeat(" ", 500))

	display.SetInputCapture(func(event *tcell.EventKey) *tcell.EventKey {
		if display.FormActive {
			return event
		}
		switch event.Key() {
		case tcell.KeyF1:
			display.Pages.SendToFront("snake")
			display.Snake.Play()
			display.Application.SetFocus(display.Snake)
			display.FormActive = true
			return nil

		case tcell.KeyF2:
			display.Pages.SendToFront("pacman")
			display.Pacman.Play()
			display.Application.SetFocus(display.Pacman)
			display.FormActive = true
			return nil
		}
		return display.ThisPage.HandleInput(event)
	})

	display.switchToPage(display.Index)

	if cfg.Projects.TokenMasterKey == config.KeyBackendUndefined {
		if security.KeyringIsAvailable() {
			cfg.Projects.TokenMasterKey = config.KeyBackendKeyring
		} else {
			cfg.Projects.TokenMasterKey = config.KeyBackendArgon2
		}
		err = config.SaveAppConfig(cfg)
		if err != nil {
			log.Infof("Failed to save application config: %s", err)
			return nil, err
		}
	}
	if cfg.Projects.TokenMasterKey == config.KeyBackendArgon2 {
		log.Info("Keyring is not available, falling back to user password")
		if cfg.Projects.Argon2Params == nil {
			display.showForm("new-password-form", display.ChoosePasswordForm)
		} else {
			display.showForm("password-form", display.PasswordForm)
		}
	} else {
		key, newKey, err := security.EnsureMasterKey()
		if err != nil {
			return nil, err
		}

		display.TokenKey = key

		err = display.loadProjects(newKey)
		if err != nil {
			return nil, err
		}
	}

	return display, nil
}

func (display *Display) loadProjects(compatToken bool) error {
	var err error
	display.Repos, err = config.LoadProjects(display.TokenKey, compatToken)
	if err != nil {
		log.Infof("Failed to load projects: %s", err)
		return err
	}

	if compatToken {
		err = config.SaveProjects(display.Repos, display.TokenKey)
		if err != nil {
			return err
		}
	}

	for _, repo := range display.Repos {
		display.Engine.AddRepository(repo)
	}

	log.Infof("Refreshing views")
	display.Projects.Refresh(display.Application, display.Repos)
	display.FilterForm.Refresh(display.Application, display.Repos)

	if len(display.Repos) == 0 {
		log.Infof("No repos, add new projet")
		display.switchToPage(display.AddProject)
		display.AddProject.LoadLocalProject()
	}

	return nil
}

func (display *Display) showForm(name string, form tview.Primitive) {
	display.FormActive = true
	display.Pages.SendToFront(name)
	display.Pages.ShowPage(name)
	display.Application.SetFocus(form)
}

func (display *Display) hideForm(name string, page tview.Primitive) {
	display.Pages.SendToBack(name)
	display.Pages.HidePage(name)
	display.Application.SetFocus(page)
	display.FormActive = false
}

func (display *Display) ChoosePasswordFormConfirm(password string) {
	key, params, err := security.GenerateMasterKeyArgon2(password)
	if err != nil {
		log.Infof("Failed to generate master key: %s", err)
		return
	}

	display.Config.Projects.Argon2Params = params

	err = config.SaveAppConfig(display.Config)
	if err != nil {
		log.Infof("Failed to save application config: %s", err)
		return
	}

	display.TokenKey = key

	display.hideForm("new-password-form", display.Index)
	display.loadProjects(false)
}

func (display *Display) PasswordFormConfirm(password string) error {
	key, err := security.ValidateMasterKeyArgon2(password, display.Config.Projects.Argon2Params)
	if err != nil {
		log.Infof("Failed to validate master key: %s", err)
		return err
	}

	display.TokenKey = key

	display.hideForm("password-form", display.Index)
	display.loadProjects(false)
	return nil
}

func (display *Display) SortFormConfirm(sorter model.MergeReqSorter) {
	display.MergeReqs.Sorter = sorter
	display.MergeReqs.ReSort()
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	display.SortFormCancel()
}

func (display *Display) SortFormCancel() {
	display.hideForm("sort-form", display.Index)
}

func (display *Display) IndexPagePickSort() {
	log.Info("Show sort")
	display.showForm("sort-form", display.SortForm)
}

func (display *Display) FilterFormConfirm(filter model.MergeReqFilter) {
	display.MergeReqs.Filter = filter
	display.MergeReqs.ReFilter()
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	display.FilterFormCancel()
}

func (display *Display) FilterFormCancel() {
	display.hideForm("filter-form", display.Index)
}

func (display *Display) IndexPagePickFilter() {
	log.Info("Show filter")
	display.showForm("filter-form", display.FilterForm)
}

func (display *Display) IndexPageQuit() {
	display.Stop()
}

func (display *Display) IndexPageViewMergeRequest(mreq model.MergeReq) {
	display.switchToPage(display.Detail)
	display.Detail.Refresh(display.Application, &mreq)
	display.Engine.MarkRead(mreq)
}

func (display *Display) IndexPageViewProjects() {
	display.switchToPage(display.Projects)
}

func (display *Display) IndexPageRefreshMergeRequests() {
	display.Engine.RefreshRepos()
}

func (display *Display) DetailPageQuit() {
	display.switchToPage(display.Index)
}

func (display *Display) DetailPageRefreshMergeRequest(mreq *model.MergeReq) {
	display.Engine.RefreshMergeRequest(*mreq)
}

func (display *Display) DetailPageAddMergeReqComment(mreq *model.MergeReq, text string, context *model.CommentContext) {
	if context == nil {
		display.Engine.AddMergeRequestComment(*mreq, text)
	} else {
		display.Engine.AddMergeRequestThread(*mreq, text, context)
	}
}

func (display *Display) DetailPageAddMergeReqReply(mreq *model.MergeReq, thread, text string) {
	display.Engine.AddMergeRequestReply(*mreq, thread, text)
}

func (display *Display) DetailPageLoadMergeReqCommitDiffs(mreq *model.MergeReq, commit *model.Commit) {
	display.Engine.LoadMergeRequestCommitDiffs(*mreq, *commit)
}

func (display *Display) DetailPageAcceptMergeReq(mreq *model.MergeReq) {
	display.Engine.AcceptMergeRequest(*mreq)
}

func (display *Display) DetailPageApproveMergeReq(mreq *model.MergeReq) {
	display.Engine.ApproveMergeRequest(*mreq)
}

func (display *Display) DetailPageUnapproveMergeReq(mreq *model.MergeReq) {
	display.Engine.UnapproveMergeRequest(*mreq)
}

func (display *Display) ProjectsPageQuit() {
	display.switchToPage(display.Index)
}

func (display *Display) ProjectsPageAddRepo() {
	log.Info("Showing add projects page")
	display.switchToPage(display.AddProject)
}

func (display *Display) AddProjectPageCancel() {
	display.switchToPage(display.Projects)
}

func (display *Display) AddProjectPageConfirm(repo model.Repo) {
	if repo.GlobalToken {
		for idx, _ := range display.Repos {
			thisrepo := &display.Repos[idx]
			if thisrepo.Server == repo.Server &&
				thisrepo.GlobalToken {
				thisrepo.Token = repo.Token
				display.Engine.UpdateRepository(*thisrepo)
			}
		}
	}

	display.Repos = append(display.Repos, repo)

	display.Projects.Refresh(display.Application, display.Repos)
	display.FilterForm.Refresh(display.Application, display.Repos)
	display.Engine.AddRepository(repo)

	err := config.SaveProjects(display.Repos, display.TokenKey)
	if err != nil {
		log.Infof("Failed to save project list: %s", err)
	}

	display.switchToPage(display.Index)
}

func (display *Display) AddProjectPageAutoFillToken(server, project string) string {
	for _, repo := range display.Repos {
		if repo.Server == server && repo.GlobalToken {
			return repo.Token
		}
	}
	return ""
}

func (display *Display) SnakeGameFinished() {
	display.switchToPage(display.Index)
}

func (display *Display) PacmanGameFinished() {
	display.switchToPage(display.Index)
}

func (display *Display) Status(msg string) {
	display.Application.QueueUpdateDraw(func() {
		display.Messages.Info(msg)
	})
}

func (display *Display) MergeRequestNotify(mreq *model.MergeReq) {
	log.Infof("Received merge request %s", mreq.String())
	display.MergeReqs.Insert(mreq)

	log.Info("Updating index page")
	display.Index.Refresh(display.Application, display.MergeReqs.Active)

	log.Info("Updating detail page")
	thatmreq := display.Index.GetSelectedMergeRequest()
	if thatmreq != nil && thatmreq.Equal(mreq) {
		display.Detail.Refresh(display.Application, mreq)
	}
}

func (display *Display) RepoAdded(repo model.Repo) {
}

func (display *Display) RepoRemoved(repo model.Repo) {
}

func (display *Display) switchToPage(page Page) {
	display.Pages.SendToFront(page.GetName())
	display.FormActive = false
	display.ThisPage = page
	display.ThisPage.Activate()
	shortcuts := fmt.Sprintf("[%s:%s]",
		GetStyleColorName(ELEMENT_SHORTCUTS_TEXT),
		GetStyleColorName(ELEMENT_SHORTCUTS_FILL)) +
		display.ThisPage.GetKeyShortcuts() +
		strings.Repeat(" ", 500)
	display.KeyShortcuts.SetText(shortcuts)
}

func (display *Display) Run() {
	display.Application.Run()
}
