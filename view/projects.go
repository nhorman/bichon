// SPDX-License-Identifier: Apache-2.0
//
// Bichon: A terminal based code review tool for GitLab
//
// Copyright (C) 2019 Red Hat, Inc.

package view

import (
	"github.com/gdamore/tcell"
	"github.com/rivo/tview"

	"gitlab.com/bichon-project/bichon/model"
)

type ProjectsPageListener interface {
	ProjectsPageQuit()
	ProjectsPageAddRepo()
}

type ProjectsPage struct {
	*tview.Frame

	Application *tview.Application
	Listener    ProjectsPageListener
	Projects    *tview.Table
}

func NewProjectsPage(app *tview.Application, listener ProjectsPageListener) *ProjectsPage {
	projects := tview.NewTable().
		SetSelectable(true, false).
		SetSelectedStyle(
			GetStyleColor(ELEMENT_PROJECTS_ACTIVE_TEXT),
			GetStyleColor(ELEMENT_PROJECTS_ACTIVE_FILL),
			GetStyleAttrMask(ELEMENT_PROJECTS_ACTIVE_ATTR))

	layout := tview.NewFrame(projects).
		SetBorders(0, 0, 0, 0, 0, 0)

	page := &ProjectsPage{
		Frame: layout,

		Application: app,
		Listener:    listener,
		Projects:    projects,
	}

	return page
}

func (page *ProjectsPage) GetName() string {
	return "projects"
}

func (page *ProjectsPage) GetKeyShortcuts() string {
	return "q:Back to index a:Add project ?:Help"
}

func (page *ProjectsPage) Refresh(app *tview.Application, repos []model.Repo) {
	go app.QueueUpdateDraw(func() {
		page.refreshMain(app, repos)
	})
}

func (page *ProjectsPage) buildRepoRow(repo *model.Repo) [2]*tview.TableCell {
	return [2]*tview.TableCell{
		&tview.TableCell{
			Text:            repo.String(),
			Reference:       repo,
			Color:           GetStyleColor(ELEMENT_PROJECTS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_PROJECTS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
		},
		&tview.TableCell{
			Text:            repo.Directory,
			Color:           GetStyleColor(ELEMENT_PROJECTS_INACTIVE_TEXT),
			BackgroundColor: GetStyleColor(ELEMENT_PROJECTS_INACTIVE_FILL),
			Align:           tview.AlignLeft,
			NotSelectable:   false,
			Expansion:       1,
		},
	}
}

func (page *ProjectsPage) refreshMain(app *tview.Application, repos []model.Repo) {
	page.Projects.Clear()

	for idx, repo := range repos {
		row := page.buildRepoRow(&repo)

		for col, val := range row {
			page.Projects.SetCell(idx, col, val)
		}
	}
}

func (page *ProjectsPage) Activate() {
	page.Application.SetFocus(page.Projects)
}

func (page *ProjectsPage) getSelectedRepo() *model.Repo {
	if page.Projects.GetRowCount() == 0 {
		return nil
	}
	row, _ := page.Projects.GetSelection()

	cell := page.Projects.GetCell(row, 0)

	ref := cell.GetReference()

	if ref == nil {
		return nil
	}

	mreq, ok := ref.(*model.Repo)
	if !ok {
		return nil
	}

	return mreq
}

func (page *ProjectsPage) HandleInput(event *tcell.EventKey) *tcell.EventKey {
	switch event.Key() {
	case tcell.KeyRune:
		switch event.Rune() {
		case 'q':
			page.Listener.ProjectsPageQuit()
		case 'a':
			page.Listener.ProjectsPageAddRepo()
		default:
			return event
		}
	default:
		return event
	}

	return nil
}
